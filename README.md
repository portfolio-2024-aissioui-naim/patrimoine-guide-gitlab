# GUIDE SUR LES COMMANDES GIT ~ Naim Aissioui

##### Avant de configurer et de faire des opérations sur Git, veuillez créer un dossier (mkdir nom_du_fichier), puis se rendre dans ce dossier (cd nom_du_fichier).

---

## Configuration utilisateur
```git
git config --global user.name "Votre nom d'utilisateur"
``` 
Définit le nom d'utilisateur que vous voulez associer à toutes vos opérations sur Git

<br>

``` git
git config --global user.email "Votre adresse email"
```  
Définit l'email que vous voulez associer à toutes vos opérations sur Git

---

## Créer des dépôts
``` git
git init
```
Crée un dépôt local dans le dossier crée

<br>

``` git
git clone le_lien_URL
```
Clone le projet existant depuis le dépôt distant vers votre dossier

``` git
git remote add origin lien_URL_du_projet_dans_le_dépôt_distant.git
```
Créer un dépot distant (Ne pas oublier le .git à la fin du lien URL)

---

## Actions sur le dépôt
```git
git add nom_du_fichier
```
Ajoute le fichier, en préparation pour le suivi de version
<br>

```git	
git add .
```
Ajoute tout le dossier, en préparation pour le suivi de version
<br>
```git
git commit -m "Votre message descriptif"
```
Enregistre le fichier ou le dossier de façon permanente dans
l'historique des versions
<br>

```git
git status
```
Liste tous les nouveaux fichiers et les fichiers modifiés à commiter
<br>

```git
git reset nom_du_fichier
```
Enleve le fichier de l'index, mais conserve son contenu
<br>

```git
git rm nom_du_fichier
```
Supprime le fichier du répertoire de travail et met à jour l'index
<br>

```git
git log
```
Montre l'historique des versions pour la branche courante

---

## Les branches
```git
git branch
```
Liste toutes les branches locales dans le dépôt courant
<br>

```git
git branch nom_de_la_branche
```
Crée une nouvelle branche
<br>

```git
git checkout nom_de_la_branche
```
Bascule sur la branche spécifiée et met à jour le répertoire de travail
<br>

```git
git merge nom_de_la_branche
```
Combine dans la branche courante l'historique de la branche spécifiée
<br>

```git
git branch -d nom_de_la_branche
```
Supprime la branche spécifiée

---

## Synchroniser les changements
```git
git fetch nom_du_dépôt
```
Récupère tout l'historique du dépôt nommé
<br>

```git
git merge nom_du_dépôt/nom_de_la_branche
```
Fusionne la branche du dépôt dans la branche locale spécifiée
<br>

```git
git push origin nom_de_la_branche
```
Envoie tous les commits de la branche locale vers GitLab ou GitHub
<br>

```git
$ git pull
```
Récupère tout l'historique du dépôt nommé et incorpore les modifications

---

## Envoyer vers le dépôt distant un dossier local qui ne contient pas Git
```git
cd dossier_existant #Se rendre dans le dossier existant
git init #Initialisation de git dans le dossier qui n'a pas été initialisé
git config --global user.name "Votre nom d'utilisateur" #Se connecter sur le dépôt distant GitLab ou GitHub
git remote add origin lien_URL_du_projet_dans_le_dépôt_distant.git #Créer un dépôt distant
git add . #Ajoute tout le dossier
git commit -m "Votre message descriptif" #Votre commit
git push origin nom_de_la_branche #Envoi de vos commits vers le dépôt distant
```

---

## Envoyer vers le dépôt distant un dossier local qui contient Git
```git
cd dossier_existant #Se rendre dans le dossier existant
git add . #Ajoute tout le dossier
git commit -m "Votre message descriptif" #Votre commit
git push origin nom_de_la_branche #Envoi de vos commits vers le dépôt distant
```
